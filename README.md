# Assessing Asset Failure - The CReDo Bayesian Network Approach

This repository contains the code used in building the Bayesian networks required for modelling infrastructure asset failure. It was designed to be adaptable to changing the definition of the network, as the elicitation process meant adaptability to learning of new mechanisms of failure was highly important. The work within this repository was developed as a component of the Climate Resilience Demonstrator (CReDo). For more information on the CReDo project, see [here](https://digitaltwinhub.co.uk/projects/credo/), and for more details on this specific component see [Technical Report 3: Assessing Asset Failure](https://digitaltwinhub.co.uk/projects/credo/technical/3-assessing-asset-failure/), also available with a [DOI](https://doi.org/10.17863/CAM.81780).

## Using the model

## Model builds


Code within this repository has been designed for integration within a [DAFNI](https://dafni.ac.uk/) workflow, by first being uploaded as a DAFNI model. This requires containerisation, and the scripts required to do this are enclosed. The model definition file is `model_definition.yaml` and the model can be prepared for upload through running
```bash make clean
make build
```
which will produce a zipped file at the location `/dafni_models/flood-impact-model.tar.gz`. Upload this and the model definition file when creating a model on DAFNI.   It may take 5-10 minutes to create the file.


## Understanding the model

The repository contains two components: a `credo_bn` module consisting of key functions for building a Bayesian network and a demonstration of implementing these tools in the `scripts` directory. The code is in python and developed using poetry package management.

The `ClassifyAssets.py` script has been written specifically to run as a component in the CReDo modular workflow.
Placing demo data and a configuration file from the `example_files` in the input location of the Dockerfile will allow you to test the operation of the model. To define your own version of the Bayesian network, it is likely best to start from the empty template present in the examples. These json files require you to identify the nodes and edges in the graph, as well as any binning used for discretisng the data. Creating models in this way allows us to use the same script for interacting with the data and transporting models easily from multiple applications.


## Local testing

To build a local image, run a shell prompt within the model container or test
the model run locally, use the included 'quick start' script:

```bash
./quick_start.sh
```
