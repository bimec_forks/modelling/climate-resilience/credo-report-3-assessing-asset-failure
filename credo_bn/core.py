"""
A collection of tools developed for building Bayesian networks within the CReDo
project.
"""


from pgmpy.models import BayesianNetwork
from pgmpy.factors.discrete import TabularCPD
import numpy as np
import json
from pgmpy.inference import VariableElimination


def assign_bin(value, bin_edges):
    """
    Take any number and assign it to a bin, returning the index of the
    bin it belongs to. Required for taking continuous data and mapping to discrete.

    Args:
        value: A continuous number to be placed into a discrete number of bins
        bin_edges: The edges of the discrete bins
    Returns:
        Integer bin number of the input value
    """
    indices = np.arange(0, len(bin_edges))
    if value < bin_edges[0]:
        return 0
    if bin_edges[-1] <= value:
        return len(bin_edges)
    mask = [False]
    _ = [
        mask.append((value < bin_edges[t]) & (bin_edges[t - 1] <= value))
        for t in range(1, len(bin_edges))
    ]
    return int(indices[mask])


def tabulate(depths, p_failure):
    """
    A function to turn flood depths and probabilities of failure into a table for
    pgmpy. As pgmpy is primarily adapted for Tabular CPDs, continuous distributions
    can cause problems. Approximating continuous probability distributions to a
    discrete number of bins allows the use of the TabularCPD functions.

    Args:
        depths: A list of bin edges to be used when tabulating the data.
        p_failure: A function mapping the coordinates in "depths" to a probability of
        failure
    Returns:
        A list with the structure [ mean failure probability in each bin,
        mean survival probability in each bin]
    """
    failure_probs = [
        ((p_failure(depths[d]) + p_failure(depths[d - 1]))) / 2
        for d in range(1, len(depths))
    ]
    survival_probs = [1.0 - a for a in failure_probs]
    return [failure_probs, survival_probs]


def p_failure(submerged_level, max_limit=0.6, min_limit=0.0):
    """
    An example function to map flood and asset level data to a probability of flooding,
    from a continuous function. This demo just uses a straight line from 0.0 probability
    below a minimum, and 1.0 above another level
    Args:
        submerged_level: The value to evaluate the probability for
        max_limit: The value above which all probabilities equal 1.0
        min_limit: the value below which all probabilities equal 0.0
    Returns:
        The probability of failure for the input depth

    """
    if submerged_level < min_limit:
        return 0.0
    if submerged_level > max_limit:
        return 1.0
    return (1.0 / max_limit) * submerged_level  # Using a linear function


def build_flood_model(binning_scheme=None):
    """
    A function to construct a pre-defined flood model. Allows customisation of the
    binning scheme used for discretising the depths of floodwater, but otherwise is
    less easily customisable as the Bayesnet class. Preserved in this code as an
    alternative pre-built model but not recommended for further use beyond basic
    testing.

    Args:
        binning_scheme: A dictionary on the nodes to discretise. Takes the form :
        {"Node name": {"min": <float>, below which probability = 0.0,
        "max":<float>, above which probability = 1.0,
        "number": <int> the number of
    Returns:
        Constructed Bayesian network using pgmpy
        Binning scheme used for discretising the data

    """
    if binning_scheme is None:
        binning_scheme = {"SubmergedLevel": {"min": 0.0, "max": 0.6, "number": 6}}

    asset_model = BayesianNetwork(
        [
            ("SubmergedLevel", "FloodFailure"),
            ("FloodFailure", "AssetFunctioning"),
            ("BackupSupply", "AssetFunctioning"),
        ]
    )

    flood_levels = np.linspace(
        binning_scheme["SubmergedLevel"]["min"],
        binning_scheme["SubmergedLevel"]["max"],
        binning_scheme["SubmergedLevel"]["number"],
    )  # bin
    # edges
    probabilities = tabulate(flood_levels, p_failure)
    labels_first = f"submerged_depth <{flood_levels[0]}"
    labels = [
        f"{str(round(flood_levels[t - 1], 3))}m <= submerged depth < "
        f"{str(round(flood_levels[t], 3))}m"
        for t in range(1, len(flood_levels))
    ]
    labels_last = f"{round(flood_levels[-1], 3)} <= submerged depth"
    all_labels = [labels_first]
    _ = [all_labels.append(a) for a in labels]
    all_labels.append(labels_last)
    all_probs = [0.0]
    _ = [all_probs.append(a) for a in probabilities[0]]
    all_probs.append(1.0)

    p_failing = all_probs
    p_not_failing = [1.0 - a for a in all_probs]

    p_level = [[1.0 / len(all_labels)] for _ in range(len(all_labels))]

    submerged_level_cpd = TabularCPD(
        variable="SubmergedLevel",
        variable_card=len(all_labels),
        values=p_level,
        state_names={"SubmergedLevel": all_labels},
    )
    failure_cpd = TabularCPD(
        variable="FloodFailure",
        variable_card=2,
        values=[p_failing, p_not_failing],
        evidence=["SubmergedLevel"],
        evidence_card=[len(all_labels)],
        state_names={
            "SubmergedLevel": all_labels,
            "FloodFailure": ["AssetAffected", "AssetUnaffected"],
        },
    )
    backup_cpd = TabularCPD(
        variable="BackupSupply",
        variable_card=2,
        values=[[0.6], [0.4]],
        state_names={"BackupSupply": ["True", "False"]},
    )
    asset_functions_cpd = TabularCPD(
        variable="AssetFunctioning",
        variable_card=2,
        evidence=["FloodFailure", "BackupSupply"],
        evidence_card=[2, 2],
        values=[[0.5, 0.0, 1.0, 0.99], [0.5, 1.0, 0.0, 0.01]],
        state_names={
            "FloodFailure": ["AssetAffected", "AssetUnaffected"],
            "BackupSupply": ["True", "False"],
            "AssetFunctioning": ["True", "False"],
        },
    )
    asset_model.add_cpds(
        submerged_level_cpd, backup_cpd, asset_functions_cpd, failure_cpd
    )

    return asset_model, binning_scheme


class Bayesnet:
    """
    A class for constructing flexible Bayesian networks and for querying the probable
    outcomes using the pgmpy package.

    Attributes:
        network: The Bayesian network used for evaluation.

    """

    def __init__(self):
        """
        Creates the Bayesnet class with default values. Does not create a network
        immediately.
        """
        self.network = None
        self.__binning = None
        return

    def build_model(self, model_name=None, model_path=None):
        """
        Constructs a Bayesian network according to specification. Can either take an
        input json specifying the file name defining the model, or load a simple
        model by name. The loaded model is saved to the class for reuse.
        Args:
            model_name: None or "flood". If "flood", calls build_flood_Model() to
            load a specific version of a Bayesian network.
            model_path: A path to a model definition file. This file must be in json
            format and define all nodes, relationships and any binning used in the network.


        """
        if model_name == "flood":
            self.network, self.binning = build_flood_model()
        elif model_path is not None:
            with open(model_path, "r") as fp:
                network_config = json.load(fp)
            model = BayesianNetwork(network_config["connections"])
            for cpd in network_config["CPDs"]:
                model.add_cpds(TabularCPD(**cpd))
            self.network = model
            self.__binning = network_config["binning"]
        return

    def discretise_variable(self, node_name, value, linspace=False):
        """Calculate the binning coordinate of a variable using the loaded Bayesian
        network
        Args:
            node_name: Node in the network represented by value
            value: number to be discretised
            linspace: Boolean to declare if regular binning is used or not in the
            binning scheme. If true, assumes numpy.linspace can be used to generate
            binning coordinates.
            Returns:
                 Integer position for the bin containing value
        """
        if linspace is True:
            edges = np.linspace(
                self.__binning[node_name]["min"],
                self.__binning[node_name]["max"],
                self.__binning[node_name]["number"],
            )
        else:
            edges = self.__binning[node_name]
        index = assign_bin(value, edges)
        return index

    def query_status(self, target_dist=[], evidence={}, method="VE"):
        """
        Using a preloaded Bayesian network, evaluate the likely output stages using
        observed evidence.
        Args:
            target_dist: List of nodes to extract probable states for
            evidence: dictionary of updates to apply to the network before querying
            likely states
            method: Approach to use for calculating likely states. Currently only
            allows "Variable elimination" from pgmpy.
        Returns:
            Resulting probability distribution from query parameters.
        """
        if method == "VE":
            infer = VariableElimination(self.network)
        return infer.query(target_dist, evidence=evidence)
