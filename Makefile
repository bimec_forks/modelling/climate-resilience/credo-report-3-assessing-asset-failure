SOURCE_FILES := $(wildcard ./*/*.py) Dockerfile
.PHONY: build clean

dafni_models/flood-impact-model.tar.gz: $(SOURCE_FILES)
	docker build -t flood-impact-model:to-upload .
	docker save flood-impact-model:to-upload | gzip > $@

build: dafni_models/flood-impact-model.tar.gz

clean:
	rm -f dafni_models/flood-impact*
