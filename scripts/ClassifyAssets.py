"""
A script used to take the tools from credo_bn and use them in the CReDo workflow on
DAFNI.
"""


import json
import os
import credo_bn.core
from glob import glob
import numpy as np


if "INPUT_LOC" in os.environ:
    KG_FOLDER = os.environ["INPUT_LOC"]
    OUTPUT_FOLDER = os.environ["OUTPUT_LOC"]
else:
    KG_FOLDER = "/data/inputs/kg_contents"
    OUTPUT_FOLDER = "/data/outputs"

INPUT_FOLDER = "/data/inputs"


def evaluate_risk(assets, bnet=None, bool_states=True, skip_failures=True):
    """
    Function for taking a Bayesian network and some asset data and returning updated
    states. Specific to the version of the CReDo workflow at the time of publication.

    Args:
        assets: an interable list of asset information
        bnet: The Bayesian network used to evaluate risk to assets
        bool_states: Whether to return a True/False indicator for asset availability
        (if True) or whether to return a continuous probability (if False)
        skip_failures: Boolean, if True the loop does not re-evaluate assets which have
        already failed, as they will not become active as soon as water recedes.
    """
    states = {}
    for asset in assets:
        flood_depth = asset["States"]["Flood depth"]["value"]
        if skip_failures:
            evaluate = bool(asset["States"]["Flood state"]["value"]) == False
        else:
            evaluate = True
        flood_bin = bnet.discretise_variable("Flood depth", flood_depth, linspace=False)
        p_failure = bnet.query_status(["Failure"], evidence={"Flood depth": flood_bin})
        if bool_states:
            f_prob = p_failure.__dict__["values"][
                p_failure.__dict__["name_to_no"]["Failure"]["Failed"]
            ]
            state_out = bool(np.random.choice([True, False], p=[f_prob, 1.0 - f_prob]))
        else:
            state_out = p_failure.__dict__["values"][
                p_failure.__dict__["name_to_no"]["Failure"]["Failed"]
            ]
        if evaluate:
            states.update({asset["States"]["Flood state"]["IRI"]: state_out})
    return states


if __name__ == "__main__":
    kg_files = glob(f"{KG_FOLDER}/*.json")
    # KG presented as a series of json files for each asset, compile to a single file:
    data_dict = []
    for file in kg_files:
        with open(file) as f:
            new_data = json.load(f)
        _ = [data_dict.append(a) for a in new_data]
    structure_file = glob(f"{INPUT_FOLDER}/structure/*.json")[0]  # Assumes a single
    # model configuration file is passed in a dataslot
    bnet = credo_bn.core.Bayesnet()
    bnet.build_model(model_path=structure_file)
    updates = evaluate_risk(data_dict, bnet=bnet)
    # Workflow expects a single json file response:
    with open(f"{OUTPUT_FOLDER}/bn_results.json", "w") as file:
        json.dump(updates, file, indent=2)
